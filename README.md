# README #

Some simple graphs for based on the files from gap.chiro.be

Version 0.1

### How do I set up? ###
####Setup

Place entire folder on a webserver

Create a folder 'data' and place there all your data files from the gap-server

####Data files

Data files come from the gap-server, so login to the gap, goto 'Ingeschreven' select the year you want the data from and change the number of people displayed to "show all"

Right-mouse button -> save file -> name it whatever you want and save the file in the 'data'-folder you created

### Help

If the graphs are not displayed correctly try refreshing the browser of (Ctr+F5)

###Made by Edward Baekelandt