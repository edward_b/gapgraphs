<?php

//read dir
$files = scandir ('./data');
$personen = array(array()); //[0] jongens, [1] meisjes, [2] leiding, [3] leden, [4] leidingVenten, [5] leidingVrouwen
$jaartallen = array();

foreach($files as $file){
	//Needed to check id it isnt the '.' of '..' dir
	if($file != '.' && $file != '..'){
//read file
$html = file('./data/'.$file);

$jongens = 0;
$meisjes = 0;
$leiding = 0;
$leden = 0;
$leidingVenten = 0;
$leidingVrouwen = 0;
$sl = 0;
$sp = 0;
$ra = 0;
$ti = 0;
$ke = 0;
$as = 0;

// Loop over all elements
for ($i = 0,  $size = count($html); $i < $size; $i++) {
    if(strstr($html[$i],"Lid")==true){
        $leden++;
        //Check if boy or girl
        if(strstr($html[$i+11],"♀")==true){
            $meisjes++;
        } else if(strstr($html[$i+11],"♂")==true){
            $jongens++;
        }

        //Check the afdeling
        if(strstr($html[$i+17],"Sl")==true){
            $sl++;
        } else if(strstr($html[$i+17],"SP")==true){
            $sp++;
        } else if(strstr($html[$i+17],"RA")==true){
            $ra++;
        } else if(strstr($html[$i+17],"TI")==true){
            $ti++;
        } else if(strstr($html[$i+17],"KE")==true){
            $ke++;
        } else if(strstr($html[$i+17],"AS")==true){
            $as++;
        }

    }
    if(strstr($html[$i],"Leiding")==true){
        $leiding++;
        if(strstr($html[$i+11],"♀")==true){
            $leidingVrouwen++;
        }
        if(strstr($html[$i+11],"♂")==true){
            $leidingVenten++;
        }
    }

}
$personen[0][] = $jongens;
$personen[1][] = $meisjes;
$personen[2][] = $leiding;
$personen[3][] = $leden;
$personen[4][] = $leidingVenten;
$personen[5][] = $leidingVrouwen;
$personen[6][] = $sl;
$personen[7][] = $sp;
$personen[8][] = $ra;
$personen[9][] = $ti;
$personen[10][] = $ke;
$personen[11][] = $as;

//Get the years (they are in element 117)
$jaartallen[] = substr($html[117], -15,9);
}
}
//print_r($personen[4]);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Overzicht Chiro Okido</title>
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript">

//Display Pie with boys vs girls
$(function () {
    $('#OverzichtJongensMeisjesHuidigWerkjaar').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: 'Verhouding jongens-meisjes, <?php $size = count($jaartallen); echo $jaartallen[$size-1];?>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Jongens vs meisjes',
            data: [
                ['Jongens', <?php echo $jongens; ?>],
                ['Meisjes',  <?php echo $meisjes; ?>],
            ]
        }]
    });

	//Display linechart with boys and girls
	 $('#OverzichtJongensMeisjes').highcharts({
	        title: {
	            text: 'Totaal aantal leiding en leden + verhouding aantal jongens en meisjes',
	            x: -20 //center
	        },
	        xAxis: {
	        	title:{
	        		text: 'Jaartal'
	        	},
	            categories: [<?php foreach($jaartallen as $jaartal){echo "'".$jaartal."',";} ?>]
	        },
	        yAxis: {
	            title: {
	                text: 'Aantal'
	            },
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#808080'
	            }]
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        series: [{
	            name: 'Jongens',
	            data: [<?php foreach($personen[0] as $aantal){echo $aantal.',';}?>]
	        }, {
	           name: 'Meisjes',
	            data: [<?php foreach($personen[1] as $aantal){echo $aantal.',';}?>]
	        },  {
               name: 'Aantal leden',
                data: [<?php foreach($personen[3] as $aantal){echo $aantal.',';}?>]
            }, {
	           name: 'Totaal aantal leden + leiding',
	            data: [<?php $totaal= array(); for ($i = 0,  $size = count($personen[2]); $i < $size; $i++) {  $totaal[]  = $personen[2][$i]+$personen[3][$i];} foreach($totaal as $t){echo $t.',';} ?>]
	        }, {
	           name: 'Aantal leiding',
	            data:  [<?php foreach($personen[2] as $aantal){echo $aantal.',';}?>]
	        }]
	    });

	//leiding vs leden columngraph
    $('#leidingvsleden').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Leiding vs Leden'
        },
        xAxis: {
             categories: [<?php foreach($jaartallen as $jaartal){echo "'".$jaartal."',";} ?>]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Aantal personen'
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Aantal leiding',
            data:  [<?php foreach($personen[2] as $aantal){echo $aantal.',';}?>]

        }, {
           name: 'Aantal leden',
            data:  [<?php foreach($personen[3] as $aantal){echo $aantal.',';}?>]
        }]
    });

	//leiding venten vs vrouwen columngraph + spline
    $('#leidingventenvrouwen').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Leiding mannen vs vrouwen'
        },labels: {
            items: [{
                html: '<?php $size = count($jaartallen); echo $jaartallen[$size-1];?>',
                style: {
                    left: '240',
                    top: '0x',
                    width: '150px',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                }
            }]
        },
        xAxis: {
             categories: [<?php foreach($jaartallen as $jaartal){echo "'".$jaartal."',";} ?>]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Aantal personen'
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Aantal Mannen',
            data:  [<?php foreach($personen[4] as $aantal){echo $aantal.',';}?>]

        }, {
           name: 'Aantal Vrouwen',
            data:  [<?php foreach($personen[5] as $aantal){echo $aantal.',';}?>]
        },{
            type: 'spline',
             name: 'Aantal leiding',
            data:  [<?php foreach($personen[2] as $aantal){echo $aantal.',';}?>]
           
        },{
            type: 'pie',
            name: 'Mannen vs Vrouwen 2014-15',
            data: [
                ['Mannen',<?php  $size = count($personen[4]); echo $personen[4][$size-1];  ?>],
                ['Vrouwen',  <?php  $size = count($personen[5]); echo $personen[5][$size-1];  ?>]
            ],
            center: [250, 25],
            size: 50,
            showInLegend: false,
            dataLabels: {
                enabled: false
            }
        }]
    });

    //leden jongens vs meisjes columngraph + spline
    $('#ledenjongensmeisjes').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Leden jongens vs meisjes'
        },labels: {
            items: [{
                html: '<?php $size = count($jaartallen); echo $jaartallen[$size-1];?>',
                style: {
                    left: '240',
                    top: '-15px',
                    width: '150px',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                }
            }]
        },
        xAxis: {
             categories: [<?php foreach($jaartallen as $jaartal){echo "'".$jaartal."',";} ?>]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Aantal personen'
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Aantal Jongens',
            data:  [<?php foreach($personen[0] as $aantal){echo $aantal.',';}?>]

        }, {
           name: 'Aantal Meisjes',
            data:  [<?php foreach($personen[1] as $aantal){echo $aantal.',';}?>]
        },{
            type: 'spline',
             name: 'Aantal leden',
            data:  [<?php foreach($personen[3] as $aantal){echo $aantal.',';}?>]
           
        },{
            type: 'pie',
            name: 'Mannen vs Vrouwen 2014-15',
            data: [
                ['Mannen',<?php  $size = count($personen[0]); echo $personen[0][$size-1];  ?>],
                ['Vrouwen',  <?php  $size = count($personen[1]); echo $personen[1][$size-1];  ?>]
            ],
            center: [250, 0],
            size: 35,
            showInLegend: false,
            dataLabels: {
                enabled: false
            }
        }]
    });

    //#leden per afdeling
    $('#ledenperafdeling').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Aantal leden per afdeling'
        },
        xAxis: {
             categories: [<?php foreach($jaartallen as $jaartal){echo "'".$jaartal."',";} ?>]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Aantal personen'
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Aantal sloebers',
            data:  [<?php foreach($personen[6] as $aantal){echo $aantal.',';}?>],
            color: 'purple'
        }, {
           name: 'Aantal speelclub',
            data:  [<?php foreach($personen[7] as $aantal){echo $aantal.',';}?>],
            color: 'yellow'
        }, {
           name: 'Aantal rakwi',
            data:  [<?php foreach($personen[8] as $aantal){echo $aantal.',';}?>],
            color: 'green'
        }, {
           name: 'Aantal tito',
            data:  [<?php foreach($personen[9] as $aantal){echo $aantal.',';}?>],
            color: 'red'
        }, {
           name: 'Aantal keti',
            data:  [<?php foreach($personen[10] as $aantal){echo $aantal.',';}?>],
            color: 'blue'
        }, {
           name: 'Aantal aspi',
            data:  [<?php foreach($personen[11] as $aantal){echo $aantal.',';}?>],
            color: 'orange'
        }]
    });

    $('#ledenperafdelingstacked').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Aantal leden per afdeling (stacked)'
        },
        subtitle: {
            text: 'Source: Wikipedia.org'
        },
        xAxis: {
             categories: [<?php foreach($jaartallen as $jaartal){echo "'".$jaartal."',";} ?>]
        },
        yAxis: {
            title: {
                text: 'Aantal leden'
            }
        },
        plotOptions: {
            area: {
                stacking: 'normal',
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666'
                }
            }
        },
        series: [{
            name: 'Aantal sloebers',
            data:  [<?php foreach($personen[6] as $aantal){echo $aantal.',';}?>],
            color: 'purple'
        }, {
           name: 'Aantal speelclub',
            data:  [<?php foreach($personen[7] as $aantal){echo $aantal.',';}?>],
            color: 'yellow'
        }, {
           name: 'Aantal rakwi',
            data:  [<?php foreach($personen[8] as $aantal){echo $aantal.',';}?>],
            color: 'green'
        }, {
           name: 'Aantal tito',
            data:  [<?php foreach($personen[9] as $aantal){echo $aantal.',';}?>],
            color: 'red'
        }, {
           name: 'Aantal keti',
            data:  [<?php foreach($personen[10] as $aantal){echo $aantal.',';}?>],
            color: 'blue'
        }, {
           name: 'Aantal aspi',
            data:  [<?php foreach($personen[11] as $aantal){echo $aantal.',';}?>],
            color: 'orange'
        }]
    });


});

		</script>


<script src="./highcharts/js/highcharts.js"></script>
<script src="./highcharts/js/modules/exporting.js"></script>
<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
<!--<div id="container" class="graph full"></div>-->
<div id="leidingvsleden" class="graph onefourth"></div>
<!--<div id="OverzichtJongensMeisjesHuidigWerkjaar" class="graph onefourth"></div>-->
<div id="OverzichtJongensMeisjes" class="graph half"></div>
<div id="ledenperafdelingstacked" class="graph onefourth"></div>
<div id="leidingventenvrouwen" class="graph onefourth"></div>
<div id="ledenjongensmeisjes" class="graph onefourth"></div>

<div id="ledenperafdeling" class="graph half"></div>
	</body>
</html>
